#include <math.h>


#define LEDpin 2

const int stepspermm = 200/8;
const float mmperstep = 1/stepspermm;
const float radius = 6.5; //centimeter
const float crosssection = PI*radius*radius;


void setup() {
  // put your setup code here, to run once:
  pinMode(LEDpin, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  //read serial for flowrate
  //flowrate = serial something something
  float flowrate = 200;
  float linearspeed = flowrate / crosssection; //cm per second
  int steprate = 10*linearspeed*stepspermm; //steps per second
  float cycletime = 1/(float)steprate; //in milliseconds
  //cycletime = 1000; //override for test
  
  Serial.println(flowrate);
  Serial.println(linearspeed);
  Serial.println(steprate);
  Serial.println(cycletime, 4);
  
  digitalWrite(LEDpin, HIGH);
  delayMicroseconds(1000000*cycletime/2);
  digitalWrite(LEDpin, LOW);
  delayMicroseconds(1000000*cycletime/2); 
}
