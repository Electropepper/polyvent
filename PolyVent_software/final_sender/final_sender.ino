#include <SPI.h>
const int cs = 10; // chip select is on pin 10
const int button_command_send_pin = 2;
int count = 0;
float flowrate = 200;
void setup() {
  Serial.begin(115200);
  SPI.setClockDivider(SPI_CLOCK_DIV4); // sets the clock to 1/4 of the arduino's frequency
  SPI.begin();
  pinMode(cs, OUTPUT);
  pinMode(button_command_send_pin, INPUT_PULLUP);
}

void loop() { 
  float cycle_time = send_cycle_time(flowrate);
  byte bytes[4];

  float2Bytes(cycle_time,&bytes[0]); 

  // Debug code, check that variables were properly loaded 
 // /*
  Serial.print("start");
  for(int x = 0; x < 4; x++)
    Serial.println(bytes[x]);
  Serial.println(cycle_time, 8);
 // */
  for(int x = 0; x<4; x++){
    digitalWrite(cs, LOW);
    SPI.transfer(bytes[x]);
    digitalWrite(cs, HIGH);
  }
  
}

float send_cycle_time(float flowRate){

  const int stepspermm = 200/8;
  const float mmperstep = 1/stepspermm;
  const float radius = 6.5; //centimeter
  const float crosssection = PI*radius*radius;
  float linearspeed = flowrate / crosssection; //cm per second
  float steprate = 10*linearspeed*stepspermm; //steps per second
  float cycletime = 1.00/steprate; //in milliseconds
  return cycletime;
}

void float2Bytes(float val, byte* bytes_array){
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;
  // Overite bytes of union with float variable
  u.float_variable = val;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}

float bytes2Float(byte* bytes_array){
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;
  //Serial.println("bytes2Float function");
  memcpy(u.temp_array, bytes_array,4);
 // Serial.println(u.float_variable);
  return(u.float_variable); 
}
