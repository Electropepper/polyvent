/*
 * PressureSensorTest
 *
 * Fetch and print values from a Honeywell 
 * TruStability HSC Pressure Sensor over SPI
 * 
 * The sensor values used in this demo are 
 * for a -15 to 15 psi gauge pressure sensor. 
 * 
 */

#include <HoneywellTruStabilitySPI.h>

const int cs_pins[6] = {14, 15, 16, 17, 21, 22};


TruStabilityPressureSensor sensor0( cs_pins[0], -1.50, 1.50 );
TruStabilityPressureSensor sensor1( cs_pins[1], -1.50, 1.50 );
TruStabilityPressureSensor sensor2( cs_pins[2], -1.50, 1.50 );
TruStabilityPressureSensor sensor3( cs_pins[3], -1.50, 1.50 );
TruStabilityPressureSensor sensor4( cs_pins[4], -1.50, 1.50 );
TruStabilityPressureSensor sensor5( cs_pins[5], -1.50, 1.50 );

void setup() {
  Serial.begin(115200); // start Serial communication
  SPI.begin(); // start SPI communication
  sensor0.begin(); // run sensor initialization
  sensor1.begin();
  sensor2.begin();
  sensor3.begin();
  sensor4.begin();
  sensor5.begin();
}

void loop() {
  // the sensor returns 0 when new data is ready
  if(sensor0.readSensor() == 0) {
    Serial.print( sensor0.pressure());
    Serial.print( ", ");
  }
  delay(10);
    if(sensor1.readSensor() == 0) {
    Serial.print( sensor1.pressure());
    Serial.print( ", ");
  }
  delay(10);
    if(sensor2.readSensor() == 0) {
    Serial.print( sensor2.pressure());
    Serial.print( ", ");
  }
  delay(10);
    if(sensor3.readSensor() == 0) {
    Serial.print( sensor3.pressure());
    Serial.print( ", ");
  }
  delay(10);
    if(sensor4.readSensor() == 0) {
    Serial.print( sensor4.pressure());
    Serial.print( ", ");
  }
  delay(10);
    if(sensor5.readSensor() == 0) {
    Serial.print( sensor5.pressure());
  }
  Serial.println(" ");
  delay(10);
}
