#include <SPI.h>
const int cs = 10; // chip select is on pin 10
//const int button_command_send_pin = 2;
int count = 0;
void setup() {
  Serial.begin(115200);
 // SPI.setClockDivider(SPI_CLOCK_DIV4); // sets the clock to 1/4 of the arduino's frequency
  SPI.begin();
  pinMode(cs, OUTPUT);
//  pinMode(button_command_send_pin, INPUT_PULLUP);
}

void loop() {
count++;
//  if(digitalRead(button_command_send_pin) == LOW){
    send_data(count);
    delay(1000);
    Serial.println("data sent");
  //}
}

void send_data(int16_t instruction){
  digitalWrite(cs, LOW);
  SPI.transfer(instruction);
  digitalWrite(cs, HIGH);
  
}
