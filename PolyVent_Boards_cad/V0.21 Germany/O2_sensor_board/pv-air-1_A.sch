EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev "A"
Comp "PolyVent"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Air sensor"
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U2
U 1 1 600B8A22
P 6350 3600
F 0 "U2" H 5900 5050 50  0000 C CNN
F 1 "ATmega328P-AU" H 6750 2150 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 6350 3600 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 6350 3600 50  0001 C CNN
	1    6350 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 600BCE1E
P 7800 3050
F 0 "Y1" V 7950 3200 50  0000 C CNN
F 1 "16MHZ" V 7650 3200 40  0000 C CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm_HandSoldering" H 7800 3050 50  0001 C CNN
F 3 "~" H 7800 3050 50  0001 C CNN
	1    7800 3050
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 600BDC6E
P 8100 2850
F 0 "C5" V 8000 2650 50  0000 L CNN
F 1 "22pF" V 8250 2750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8138 2700 50  0001 C CNN
F 3 "~" H 8100 2850 50  0001 C CNN
	1    8100 2850
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 600BE461
P 8100 3250
F 0 "C6" V 8000 3350 50  0000 L CNN
F 1 "22pF" V 8250 3150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8138 3100 50  0001 C CNN
F 3 "~" H 8100 3250 50  0001 C CNN
	1    8100 3250
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 600BE985
P 6700 1850
F 0 "C3" V 6550 1800 50  0000 L CNN
F 1 "100nF" V 6850 1750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6738 1700 50  0001 C CNN
F 3 "~" H 6700 1850 50  0001 C CNN
	1    6700 1850
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 600BED4D
P 5150 2750
F 0 "C2" H 5265 2796 50  0000 L CNN
F 1 "100nF" H 5250 2650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5188 2600 50  0001 C CNN
F 3 "~" H 5150 2750 50  0001 C CNN
	1    5150 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 600E4DA5
P 8400 3350
F 0 "#PWR0101" H 8400 3100 50  0001 C CNN
F 1 "GND" H 8405 3177 50  0000 C CNN
F 2 "" H 8400 3350 50  0001 C CNN
F 3 "" H 8400 3350 50  0001 C CNN
	1    8400 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3000 7500 3000
Wire Wire Line
	7500 3000 7500 2850
Wire Wire Line
	7500 2850 7800 2850
Wire Wire Line
	6950 3100 7500 3100
Wire Wire Line
	7500 3100 7500 3250
Wire Wire Line
	7500 3250 7800 3250
Wire Wire Line
	7800 3200 7800 3250
Connection ~ 7800 3250
Wire Wire Line
	7800 3250 7950 3250
Wire Wire Line
	7800 2900 7800 2850
Connection ~ 7800 2850
Wire Wire Line
	7800 2850 7950 2850
Wire Wire Line
	8250 2850 8400 2850
Wire Wire Line
	8400 2850 8400 3250
Wire Wire Line
	8250 3250 8400 3250
Connection ~ 8400 3250
Wire Wire Line
	8400 3250 8400 3350
$Comp
L power:GND #PWR0102
U 1 1 600E6383
P 5150 3000
F 0 "#PWR0102" H 5150 2750 50  0001 C CNN
F 1 "GND" H 5155 2827 50  0000 C CNN
F 2 "" H 5150 3000 50  0001 C CNN
F 3 "" H 5150 3000 50  0001 C CNN
	1    5150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2600 5150 2400
Wire Wire Line
	5150 2400 5750 2400
Wire Wire Line
	5150 2900 5150 3000
$Comp
L power:+5V #PWR0103
U 1 1 600E7093
P 6350 1800
F 0 "#PWR0103" H 6350 1650 50  0001 C CNN
F 1 "+5V" H 6365 1973 50  0000 C CNN
F 2 "" H 6350 1800 50  0001 C CNN
F 3 "" H 6350 1800 50  0001 C CNN
	1    6350 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 600E8C1D
P 6950 1950
F 0 "#PWR0104" H 6950 1700 50  0001 C CNN
F 1 "GND" H 6955 1777 50  0000 C CNN
F 2 "" H 6950 1950 50  0001 C CNN
F 3 "" H 6950 1950 50  0001 C CNN
	1    6950 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1800 6350 1850
Wire Wire Line
	6450 2100 6450 2000
Wire Wire Line
	6450 2000 6350 2000
Connection ~ 6350 2000
Wire Wire Line
	6350 2000 6350 2100
Wire Wire Line
	6350 1850 6550 1850
Connection ~ 6350 1850
Wire Wire Line
	6350 1850 6350 2000
Wire Wire Line
	6850 1850 6950 1850
Wire Wire Line
	6950 1850 6950 1950
$Comp
L power:+5V #PWR0105
U 1 1 600EB0B6
P 8400 1850
F 0 "#PWR0105" H 8400 1700 50  0001 C CNN
F 1 "+5V" H 8415 2023 50  0000 C CNN
F 2 "" H 8400 1850 50  0001 C CNN
F 3 "" H 8400 1850 50  0001 C CNN
	1    8400 1850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 600ECB0F
P 8100 4000
F 0 "R7" H 8150 4100 50  0000 L CNN
F 1 "10K" H 8170 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8030 4000 50  0001 C CNN
F 3 "~" H 8100 4000 50  0001 C CNN
	1    8100 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 600EEFD5
P 8100 4750
F 0 "#PWR0106" H 8100 4500 50  0001 C CNN
F 1 "GND" H 8105 4577 50  0000 C CNN
F 2 "" H 8100 4750 50  0001 C CNN
F 3 "" H 8100 4750 50  0001 C CNN
	1    8100 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 4650 8100 4750
Wire Wire Line
	8100 4150 8100 4200
$Comp
L power:+5V #PWR0107
U 1 1 600F02B5
P 8100 3750
F 0 "#PWR0107" H 8100 3600 50  0001 C CNN
F 1 "+5V" H 8115 3923 50  0000 C CNN
F 2 "" H 8100 3750 50  0001 C CNN
F 3 "" H 8100 3750 50  0001 C CNN
	1    8100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3750 8100 3850
Wire Wire Line
	6950 3900 7750 3900
Wire Wire Line
	7750 3900 7750 4200
Wire Wire Line
	7750 4200 8100 4200
Connection ~ 8100 4200
Wire Wire Line
	8100 4200 8100 4250
Wire Wire Line
	8100 4200 8550 4200
Text Label 8300 4200 0    50   ~ 0
RESET
$Comp
L power:GND #PWR0109
U 1 1 600FECFC
P 6350 5200
F 0 "#PWR0109" H 6350 4950 50  0001 C CNN
F 1 "GND" H 6355 5027 50  0000 C CNN
F 2 "" H 6350 5200 50  0001 C CNN
F 3 "" H 6350 5200 50  0001 C CNN
	1    6350 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5100 6350 5200
$Comp
L power:+5V #PWR0110
U 1 1 60131C46
P 2300 4150
F 0 "#PWR0110" H 2300 4000 50  0001 C CNN
F 1 "+5V" H 2315 4323 50  0000 C CNN
F 2 "" H 2300 4150 50  0001 C CNN
F 3 "" H 2300 4150 50  0001 C CNN
	1    2300 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4250 2300 4150
NoConn ~ 5750 2600
NoConn ~ 5750 2700
NoConn ~ 6950 2400
NoConn ~ 6950 2500
NoConn ~ 6950 2600
NoConn ~ 6950 3300
NoConn ~ 6950 3400
NoConn ~ 6950 3500
NoConn ~ 6950 3600
NoConn ~ 6950 4300
NoConn ~ 6950 4400
NoConn ~ 6950 4500
NoConn ~ 6950 4600
NoConn ~ 6950 4700
NoConn ~ 6950 4800
$Comp
L Device:C C4
U 1 1 600E1453
P 7750 4550
F 0 "C4" H 7650 4450 50  0000 L CNN
F 1 "100nF" H 7550 4650 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7788 4400 50  0001 C CNN
F 3 "~" H 7750 4550 50  0001 C CNN
	1    7750 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	7750 4400 7750 4200
Connection ~ 7750 4200
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 6010A4F4
P 8100 5300
F 0 "J4" H 8100 5500 50  0000 C CNN
F 1 "Programming" H 8200 5000 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B4B-XH-AM_1x04_P2.50mm_Vertical" H 8100 5300 50  0001 C CNN
F 3 "~" H 8100 5300 50  0001 C CNN
	1    8100 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4700 7750 5200
Wire Wire Line
	7750 5200 7900 5200
Wire Wire Line
	6950 4100 7500 4100
Wire Wire Line
	7500 4100 7500 5300
Wire Wire Line
	7500 5300 7900 5300
Wire Wire Line
	6950 4200 7400 4200
Wire Wire Line
	7400 4200 7400 5400
Wire Wire Line
	7400 5400 7900 5400
Text Notes 8200 5350 0    50   ~ 0
RX
Text Notes 8200 5450 0    50   ~ 0
TX
Text Notes 8200 5250 0    50   ~ 0
DTR/Reset
Wire Wire Line
	7500 3800 6950 3800
Wire Wire Line
	6950 3700 7500 3700
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J3
U 1 1 600EA7F5
P 7650 2050
F 0 "J3" H 7700 2250 50  0000 C CNN
F 1 "ISP / SPI" H 7700 1850 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B6B-XH-AM_1x06_P2.50mm_Vertical" H 7650 2050 50  0001 C CNN
F 3 "~" H 7650 2050 50  0001 C CNN
	1    7650 2050
	1    0    0    -1  
$EndComp
Text Label 8050 2150 0    50   ~ 0
RESET
$Comp
L Switch:SW_Push SW1
U 1 1 600EDC47
P 8100 4450
F 0 "SW1" V 8200 4400 50  0000 R CNN
F 1 "SW_Push" V 7950 4400 50  0000 R CNN
F 2 "electropepper:B3FS-10" H 8100 4650 50  0001 C CNN
F 3 "~" H 8100 4650 50  0001 C CNN
	1    8100 4450
	0    -1   -1   0   
$EndComp
$Comp
L Analog_ADC:ADS1014IDGS U1
U 1 1 601F2EB4
P 3000 2900
F 0 "U1" H 3100 3350 50  0000 C CNN
F 1 "ADS1014IDGS" H 3300 2550 50  0000 C CNN
F 2 "Package_SO:TSSOP-10_3x3mm_P0.5mm" H 3000 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ads1015.pdf" H 2950 2000 50  0001 C CNN
	1    3000 2900
	1    0    0    -1  
$EndComp
Text Label 7300 3700 0    50   ~ 0
SDA
Text Label 7300 3800 0    50   ~ 0
SCL
Wire Wire Line
	3400 3000 4150 3000
Text Label 4350 3000 0    50   ~ 0
SDA
Text Label 4350 2900 0    50   ~ 0
SCL
$Comp
L power:GND #PWR0112
U 1 1 601FFA18
P 3000 3400
F 0 "#PWR0112" H 3000 3150 50  0001 C CNN
F 1 "GND" H 3005 3227 50  0000 C CNN
F 2 "" H 3000 3400 50  0001 C CNN
F 3 "" H 3000 3400 50  0001 C CNN
	1    3000 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 601FFFF0
P 3000 2100
F 0 "#PWR0113" H 3000 1950 50  0001 C CNN
F 1 "+5V" H 3015 2273 50  0000 C CNN
F 2 "" H 3000 2100 50  0001 C CNN
F 3 "" H 3000 2100 50  0001 C CNN
	1    3000 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3300 3000 3400
Wire Wire Line
	3000 2100 3000 2200
$Comp
L Device:R R2
U 1 1 60205E9A
P 3550 2450
F 0 "R2" H 3620 2496 50  0000 L CNN
F 1 "10K" H 3620 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 2450 50  0001 C CNN
F 3 "~" H 3550 2450 50  0001 C CNN
	1    3550 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0114
U 1 1 6020A2C9
P 3850 2100
F 0 "#PWR0114" H 3850 1950 50  0001 C CNN
F 1 "+5V" H 3865 2273 50  0000 C CNN
F 2 "" H 3850 2100 50  0001 C CNN
F 3 "" H 3850 2100 50  0001 C CNN
	1    3850 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2600 3550 2700
Wire Wire Line
	3550 2700 3400 2700
$Comp
L Device:R R4
U 1 1 6020DC35
P 3700 3450
F 0 "R4" H 3770 3496 50  0000 L CNN
F 1 "10K" H 3770 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3630 3450 50  0001 C CNN
F 3 "~" H 3700 3450 50  0001 C CNN
	1    3700 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 6020DFFB
P 3700 3700
F 0 "#PWR0115" H 3700 3450 50  0001 C CNN
F 1 "GND" H 3705 3527 50  0000 C CNN
F 2 "" H 3700 3700 50  0001 C CNN
F 3 "" H 3700 3700 50  0001 C CNN
	1    3700 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3100 3700 3100
Wire Wire Line
	3700 3100 3700 3300
Wire Wire Line
	3700 3600 3700 3700
$Comp
L power:GND #PWR0116
U 1 1 60215785
P 2200 3100
F 0 "#PWR0116" H 2200 2850 50  0001 C CNN
F 1 "GND" H 2205 2927 50  0000 C CNN
F 2 "" H 2200 3100 50  0001 C CNN
F 3 "" H 2200 3100 50  0001 C CNN
	1    2200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3000 2200 3000
Wire Wire Line
	2200 3000 2200 3100
$Comp
L Device:R R1
U 1 1 60217B3F
P 2300 2550
F 0 "R1" H 2100 2650 50  0000 L CNN
F 1 "0R" H 2100 2500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2230 2550 50  0001 C CNN
F 3 "~" H 2300 2550 50  0001 C CNN
	1    2300 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 60217FD5
P 2300 2300
F 0 "#PWR0117" H 2300 2150 50  0001 C CNN
F 1 "+5V" H 2315 2473 50  0000 C CNN
F 2 "" H 2300 2300 50  0001 C CNN
F 3 "" H 2300 2300 50  0001 C CNN
	1    2300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2700 2300 2800
Wire Wire Line
	2300 2800 2600 2800
Wire Wire Line
	2300 2300 2300 2400
$Comp
L Device:R R5
U 1 1 60237354
P 3850 2450
F 0 "R5" H 3920 2496 50  0000 L CNN
F 1 "4K7" H 3920 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3780 2450 50  0001 C CNN
F 3 "~" H 3850 2450 50  0001 C CNN
	1    3850 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 6023784E
P 4150 2450
F 0 "R6" H 4220 2496 50  0000 L CNN
F 1 "4K7" H 4220 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4080 2450 50  0001 C CNN
F 3 "~" H 4150 2450 50  0001 C CNN
	1    4150 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2600 3850 2900
Wire Wire Line
	3400 2900 3850 2900
Connection ~ 3850 2900
Wire Wire Line
	3850 2900 4600 2900
Wire Wire Line
	4150 2600 4150 3000
Connection ~ 4150 3000
Wire Wire Line
	4150 3000 4600 3000
Wire Wire Line
	3550 2300 3550 2200
Wire Wire Line
	3550 2200 3850 2200
Wire Wire Line
	4150 2200 4150 2300
Wire Wire Line
	3850 2300 3850 2200
Connection ~ 3850 2200
Wire Wire Line
	3850 2200 4150 2200
Wire Wire Line
	3850 2100 3850 2200
$Comp
L Device:C C1
U 1 1 6024F925
P 2700 2200
F 0 "C1" V 2550 2150 50  0000 L CNN
F 1 "100nF" V 2750 2250 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2738 2050 50  0001 C CNN
F 3 "~" H 2700 2200 50  0001 C CNN
	1    2700 2200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 6025DCE1
P 2500 2250
F 0 "#PWR0118" H 2500 2000 50  0001 C CNN
F 1 "GND" H 2505 2077 50  0000 C CNN
F 2 "" H 2500 2250 50  0001 C CNN
F 3 "" H 2500 2250 50  0001 C CNN
	1    2500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2200 2500 2200
Wire Wire Line
	2500 2200 2500 2250
Wire Wire Line
	2850 2200 3000 2200
Connection ~ 3000 2200
Wire Wire Line
	3000 2200 3000 2400
$Comp
L power:GND #PWR0119
U 1 1 6025D5F4
P 7800 5600
F 0 "#PWR0119" H 7800 5350 50  0001 C CNN
F 1 "GND" H 7805 5427 50  0000 C CNN
F 2 "" H 7800 5600 50  0001 C CNN
F 3 "" H 7800 5600 50  0001 C CNN
	1    7800 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 5500 7800 5600
Wire Wire Line
	7800 5500 7900 5500
Text Notes 8200 5550 0    50   ~ 0
GND
$Comp
L Device:LED D1
U 1 1 6027CDB4
P 3150 4250
F 0 "D1" H 3200 4100 50  0000 C CNN
F 1 "Power LED" H 3200 4350 45  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 4250 50  0001 C CNN
F 3 "~" H 3150 4250 50  0001 C CNN
	1    3150 4250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 6027E20E
P 2650 4250
F 0 "R3" V 2550 4150 50  0000 L CNN
F 1 "330R" V 2750 4150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2580 4250 50  0001 C CNN
F 3 "~" H 2650 4250 50  0001 C CNN
	1    2650 4250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 60285336
P 3400 4350
F 0 "#PWR0120" H 3400 4100 50  0001 C CNN
F 1 "GND" H 3405 4177 50  0000 C CNN
F 2 "" H 3400 4350 50  0001 C CNN
F 3 "" H 3400 4350 50  0001 C CNN
	1    3400 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4250 3400 4250
Wire Wire Line
	3400 4250 3400 4350
Wire Wire Line
	2800 4250 3000 4250
Wire Wire Line
	2300 4250 2500 4250
Wire Wire Line
	2100 2900 2600 2900
Wire Wire Line
	2100 2800 2300 2800
Connection ~ 2300 2800
$Comp
L power:GND #PWR0108
U 1 1 600F226D
P 7400 2250
F 0 "#PWR0108" H 7400 2000 50  0001 C CNN
F 1 "GND" H 7405 2077 50  0000 C CNN
F 2 "" H 7400 2250 50  0001 C CNN
F 3 "" H 7400 2250 50  0001 C CNN
	1    7400 2250
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 60211FAC
P 1900 2900
F 0 "J1" H 1900 3100 50  0000 C CNN
F 1 "Sensor" H 1850 2700 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B3B-XH-AM_1x03_P2.50mm_Vertical" H 1900 2900 50  0001 C CNN
F 3 "~" H 1900 2900 50  0001 C CNN
	1    1900 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7450 1950 7400 1950
Wire Wire Line
	7400 1950 7400 2250
Wire Wire Line
	7950 1950 8400 1950
Wire Wire Line
	8400 1950 8400 1850
Wire Wire Line
	6950 2900 7150 2900
Wire Wire Line
	7150 2900 7150 2050
Wire Wire Line
	7150 2050 7450 2050
Wire Wire Line
	6950 2700 8400 2700
Wire Wire Line
	8400 2700 8400 2050
Wire Wire Line
	8400 2050 7950 2050
Wire Wire Line
	6950 2800 7250 2800
Wire Wire Line
	7250 2800 7250 2150
Wire Wire Line
	7250 2150 7450 2150
Wire Wire Line
	7950 2150 8300 2150
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 6031516B
P 2300 5200
F 0 "H1" H 2400 5249 50  0000 L CNN
F 1 "MountingHole" H 2400 5158 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 2300 5200 50  0001 C CNN
F 3 "~" H 2300 5200 50  0001 C CNN
	1    2300 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60316387
P 2300 5400
F 0 "#PWR01" H 2300 5150 50  0001 C CNN
F 1 "GND" H 2305 5227 50  0000 C CNN
F 2 "" H 2300 5400 50  0001 C CNN
F 3 "" H 2300 5400 50  0001 C CNN
	1    2300 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6031665D
P 3050 5400
F 0 "#PWR02" H 3050 5150 50  0001 C CNN
F 1 "GND" H 3055 5227 50  0000 C CNN
F 2 "" H 3050 5400 50  0001 C CNN
F 3 "" H 3050 5400 50  0001 C CNN
	1    3050 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 60316801
P 3800 5400
F 0 "#PWR03" H 3800 5150 50  0001 C CNN
F 1 "GND" H 3805 5227 50  0000 C CNN
F 2 "" H 3800 5400 50  0001 C CNN
F 3 "" H 3800 5400 50  0001 C CNN
	1    3800 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 5300 2300 5400
Wire Wire Line
	3050 5300 3050 5400
Wire Wire Line
	3800 5300 3800 5400
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 60320229
P 3050 5200
F 0 "H2" H 3150 5249 50  0000 L CNN
F 1 "MountingHole" H 3150 5158 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 3050 5200 50  0001 C CNN
F 3 "~" H 3050 5200 50  0001 C CNN
	1    3050 5200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 603204A0
P 3800 5200
F 0 "H3" H 3900 5249 50  0000 L CNN
F 1 "MountingHole" H 3900 5158 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 3800 5200 50  0001 C CNN
F 3 "~" H 3800 5200 50  0001 C CNN
	1    3800 5200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 602C8A17
P 4550 5200
F 0 "H4" H 4650 5249 50  0000 L CNN
F 1 "MountingHole" H 4650 5158 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 4550 5200 50  0001 C CNN
F 3 "~" H 4550 5200 50  0001 C CNN
	1    4550 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 602C8CE3
P 4550 5400
F 0 "#PWR04" H 4550 5150 50  0001 C CNN
F 1 "GND" H 4555 5227 50  0000 C CNN
F 2 "" H 4550 5400 50  0001 C CNN
F 3 "" H 4550 5400 50  0001 C CNN
	1    4550 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 5300 4550 5400
$EndSCHEMATC
