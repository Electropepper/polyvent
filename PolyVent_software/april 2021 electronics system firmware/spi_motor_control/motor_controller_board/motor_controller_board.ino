#include "Arduino.h"
#include "SPI.h"
#include "MultiStepper.h"
#include "AccelStepper.h"
byte received_signal_raw_bytes[14];
uint16_t received_signal_reconstructed[7] = {0, 0, 0, 0, 0, 0, 0};
uint16_t old_signal_reconstructed[7];
volatile byte indx;
volatile boolean process;
byte homestatus1, homestatus2;
long stepper1PosHome, stepper2PosHome;

//permanant pin mapping order: step, dir, enable

const int pins_for_stepper_1[3] = {4, 5, 6};
const int pins_for_stepper_2[3] = {7, 8, 9};
const int switch_pins[2] = {2, 3};
volatile byte switch_states[2];

AccelStepper stepper1(1, 4, 5); // 1 is the step dir driver mode, then step pin, then dir pin
AccelStepper stepper2(1, 7, 8); // 1 is the step dir driver mode, then step pin, then dir pin

// condition statements
// bit 1: enable 1, bit 2: enable 2, bit 3: start_home, bit 4: is it homed?

void setup(void)
{
  Serial.begin(115200);

  pinMode(MISO, OUTPUT); // have to send on master in so it set as output

  SPCR |= _BV(SPE);      // turn on SPI in slave mode
  SPI.attachInterrupt(); // turn on interrupt
  indx = 0;              // received_signal_raw_bytes scal
  process = false;

  pinMode(switch_pins, INPUT);
  pinMode(pins_for_stepper_1, OUTPUT);
  pinMode(pins_for_stepper_2, OUTPUT);

  stepper1.setMaxSpeed(2000);
  stepper2.setMaxSpeed(2000);

  attachInterrupt(digitalPinToInterrupt(switch_pins[0]), switch_state_stepper_1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(switch_pins[1]), switch_state_stepper_2, CHANGE);
}

void loop(void)
{
  for (int x = 0; x < 4; x++)
  {
    received_signal_reconstructed[x] = readuint16_t((x * 2), received_signal_raw_bytes);
    if (received_signal_reconstructed[x] != old_signal_reconstructed[x]) {
      //    Serial.print("saw a change in int #: ");
      //    Serial.print(" ");
      //    Serial.println(received_signal_reconstructed[x]);
      switch (x) {
        case 0:
          stepper1.setAcceleration(received_signal_reconstructed[0]);
          break;

        case 1:
          stepper2.setAcceleration(received_signal_reconstructed[1]);
          break;

        case 2:
          stepper1.setSpeed(received_signal_reconstructed[2]);
          break;

        case 3:
          stepper2.setSpeed(received_signal_reconstructed[3]);
          break;

          //        case 4:
          //          stepper1.moveTo(received_signal_reconstructed[4]);
          //          break;
          //
          //        case 5:
          //          stepper2.moveTo(received_signal_reconstructed[5]);
          //          break;
      }
      old_signal_reconstructed[x] = received_signal_reconstructed[x];
    }
  }
  stepper1.moveTo(received_signal_reconstructed[4]);
  stepper2.moveTo(received_signal_reconstructed[5]);

  digitalWrite(pins_for_stepper_1[2], bitRead(received_signal_raw_bytes[12], 7));
  digitalWrite(pins_for_stepper_2[2], bitRead(received_signal_raw_bytes[12], 6));
  //looks for home command stepper 1
  if (bitRead(received_signal_raw_bytes[12], 5) == true)
    homestepper1();
  else if (bitRead(received_signal_raw_bytes[12], 4) == true)
    homestepper2();
  else
  {
    Serial.println("r");
    stepper1.run();
    stepper2.run();
  }
  // if (bitRead(received_signal_raw_bytes[12], 3) == true)
  //    SPDR = switch_states[0];
  //  else if (bitRead(received_signal_raw_bytes[12], 2) == true)
  //   SPDR = switch_states[1];
}

ISR(SPI_STC_vect) // SPI interrupt routine
{
  byte c = SPDR; // read byte from SPI Data Register
  if (c == '\n')
    indx = 0;
  if (indx < sizeof received_signal_raw_bytes) {
    received_signal_raw_bytes[indx] = c; // save data in the next index in the array received_signal_raw_bytes
    indx++;
  }
}

uint16_t readuint16_t(int loc_in_buffer, byte *buff)
{
  int n = loc_in_buffer;
  uint16_t v;
  *(((byte *)&v)) = buff[n];
  *(((byte *)&v) + 1) = buff[n + 1];
  return v;
}

void homestepper1()
{
  while (switch_states[0] == 0) {
    stepper1.runSpeed();
    SPDR = 0;
  }
  SPDR = 1;
  bitWrite(received_signal_raw_bytes[12], 5, false);
  stepper1.setCurrentPosition(0);
  //  stepper1.setSpeed(received_signal_reconstructed[2]);
  //  stepper1.setAcceleration(received_signal_reconstructed[0]);
  // stepper1.setSpeed(500);
  // stepper1.setAcceleration(200);
  // stepper1.moveTo(5000);
}

void homestepper2()
{
  while (switch_states[1] == 0) {
    stepper2.runSpeed();
    SPDR = 0;
  }
  SPDR = 1;
  bitWrite(received_signal_raw_bytes[12], 4, false);
  stepper2.setCurrentPosition(0);
  //  stepper2.setSpeed(received_signal_reconstructed[3]);
  //  stepper2.setAcceleration(received_signal_reconstructed[1]);
  //  stepper2.setSpeed(500);
  // stepper2.setAcceleration(200);

}

void switch_state_stepper_1() {
  switch_states[0] = digitalRead(switch_pins[0]);
}
void switch_state_stepper_2() {
  switch_states[1] = digitalRead(switch_pins[1]);
}
