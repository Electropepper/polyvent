#include <SPI.h>
const int cs = 10; // chip select is on pin 10
const int button_command_send_pin = 2;
int count = 0;
float flowrate = 200;
void setup() {
  Serial.begin(115200);
  SPI.setClockDivider(SPI_CLOCK_DIV4); // sets the clock to 1/4 of the arduino's frequency
  SPI.begin();
  pinMode(cs, OUTPUT);
  pinMode(button_command_send_pin, INPUT_PULLUP);
}

void loop() {
  /* 
  byte float_packaged[4];
  float buff = {1578};
  memcpy(float_packaged, &buff, 4);
  */
  
  float float_example = 68.23056;
  byte bytes[4] = {0,0,0,0};
  float float_final = 0.0;


  float2Bytes(float_example,&bytes[0]); 

  /* 
  Serial.print("start");
  for(int x = 0; x < 4; x++)
    Serial.println(bytes[x]);
  Serial.println(float_example); */

  Serial.print("reconverted float: ");
  Serial.println(bytes2Float(&bytes[0])); 

  
//  digitalWrite(cs, LOW);
//  SPI.transfer(float_packaged, sizeof float_packaged);
//  digitalWrite(cs, HIGH);
  
}

float send_cycle_time(float flowRate){

  const int stepspermm = 200/8;
  const float mmperstep = 1/stepspermm;
  const float radius = 6.5; //centimeter
  const float crosssection = PI*radius*radius;

  float flowrate = 200;
  float linearspeed = flowrate / crosssection; //cm per second
  float steprate = 10*linearspeed*stepspermm; //steps per second
  float cycletime = 1.00/steprate; //in milliseconds
  return cycletime;
}

void float2Bytes(float val, byte* bytes_array){
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;
  // Overite bytes of union with float variable
  u.float_variable = val;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}

float bytes2Float(byte* bytes_array){
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;
  //Serial.println("bytes2Float function");
  memcpy(u.temp_array, bytes_array,4);
 // Serial.println(u.float_variable);
  return(u.float_variable); 
}
