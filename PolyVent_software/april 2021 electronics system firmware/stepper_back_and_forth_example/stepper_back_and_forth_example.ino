#include "SPI.h"

const int port[12] = {2, 4, 5, 13, 14, 15, 16, 17, 21, 22, 25, 26};

const int CS_valves = port[0];
const int CS_motors = port[1];
bool valves[10] = {0, 1, 1, 1, 0, 0, 0, 0, 0, 1};
uint16_t all_zero[6] = {0, 0, 0, 0, 0, 0};
uint16_t motor_vals_0[6] = {100, 100, 200, 200, 0, 0};
uint16_t motor_vals_1[6] = {200, 200, 1000, 1000, 5000, 0};
uint16_t motor_vals_2[6] = {200, 200, 1000, 1000, 0, 0};
uint16_t motor_vals_3[6] = {201, 201, 1001, 1001, 4999, 0};
byte status1, status2;

void setup()
{
  Serial.begin(115200);
  SPI.begin();
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
  for (int x = 0; x < 12; x++) {
    pinMode(port[x], OUTPUT);
    digitalWrite(port[x], HIGH);
  }
  motor_board_command(motor_vals_0, 1, 1, 1, 0, 1, 0); //homes bellow
  Serial.println("sent home command!");
  delay(5000);
  motor_board_command(motor_vals_2, 1, 1, 0, 0, 0, 0); //stops homing, turns on fast mode
  Serial.println("homed!");
}

void loop()
{
  motor_board_command(motor_vals_1, 1, 1, 0, 0, 0, 0); //goes to 5000 steps
  motor_board_command(motor_vals_3, 1, 1, 0, 0, 0, 0); //goes to 5000 steps 
}

void motor_board_command(uint16_t transmission[6], bool enable1, bool enable2, bool home1, bool home2, bool home_status_1, bool home_status_2)
{
  // this is the format of what is being sent: int16 accel1, int16 accel2, int16 speed1, int16 speed2, int16 position_setpoint, int16 position_setpoint2, int16 conditions_to_ask_for_1_and_2
  byte conditionals;
  bitWrite(conditionals, 7, enable1);
  bitWrite(conditionals, 6, enable2);
  bitWrite(conditionals, 5, home1);
  bitWrite(conditionals, 4, home2);
  bitWrite(conditionals, 3, home_status_1);
  bitWrite(conditionals, 2, home_status_2);

  digitalWrite(CS_motors, LOW);

  for (int x = 0; x < 6; x++)
  {
    send_uint16_t(transmission[x]);
  }
  SPI.transfer(conditionals);

  digitalWrite(CS_motors, HIGH);
}

void valve_board_command(bool * valve_states)
{
  byte c1, c2;
  for (int x = 0; x < 8; x++)
    bitWrite(c1, x, valve_states[x]);
  for (int x = 8; x < 10; x++)
    bitWrite(c2, x - 8, valve_states[x]);

  digitalWrite(CS_valves, LOW);
  SPI.transfer(c1);
  SPI.transfer(c2);
  digitalWrite(CS_valves, HIGH);
}

void send_uint16_t(uint16_t u)
{
  // Robert Read's code
  SPI.transfer(*(((byte *)&u) + 0));
  byte b1 = *(((byte *)&u) + 0);

  SPI.transfer(*(((byte *)&u) + 1));
  byte b2 = *(((byte *)&u) + 1);
}
