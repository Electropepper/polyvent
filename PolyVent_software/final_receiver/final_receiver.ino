
#include<SPI.h>
volatile boolean received;
volatile byte received_values[4];
volatile int send_data;
int buttonvalue;
volatile int val = 0;
// cs pin is pin 2
const int led_pin = 7;
void setup(){
  
  Serial.begin(115200);
  
  pinMode(MISO,OUTPUT);                   //Sets MISO as OUTPUT (Have to Send data to Master IN 
  pinMode(led_pin, OUTPUT);
  SPCR |= _BV(SPE);                       //Turn on SPI in Slave Mode
  received = false;
  SPI.attachInterrupt();                  //Interuupt ON is set for SPI commnucation
  
}

ISR (SPI_STC_vect)                        //Inerrput routine function 
{
  if(val < 4){
    received_values[val] = SPDR;         // Value received from master if store in variable slavereceived
    val++;
  }
  else
    received = true;                        //Sets received as True 
}

void loop(){
  if(received){

    //Debug sequence: did we receive the right variables
    ///*
    Serial.println("start of sequence");
    for(int x = 0; x < 4; x++)
      Serial.println(received_values[x]);
    //*/
    
    received = false;
    // extract the original float from byte array
    float cycle_time = bytes2Float(&received_values[0]);
    Serial.println(cycle_time, 8);

    // display with the led
    digitalWrite(led_pin, HIGH);
    delay(30000*cycle_time/2);
    digitalWrite(led_pin, LOW);
    delay(30000*cycle_time/2); 
  }
}


float bytes2Float(byte* bytes_array){
  // Create union of shared memory space
  union {
    float float_variable;
    byte temp_array[4];
  } u;
  Serial.println("bytes2Float function");
  memcpy(u.temp_array, bytes_array, 4);
  Serial.println(u.float_variable, 8);
  return(u.float_variable); 
}
