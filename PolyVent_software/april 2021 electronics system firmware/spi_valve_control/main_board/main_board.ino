#include "SPI.h"
const int CS = 2; // 15


void setup() {
   Serial.begin(9600);
   pinMode(CS, OUTPUT);
   digitalWrite(CS, HIGH);
   SPI.begin();
  SPI.beginTransaction(SPISettings(10000, MSBFIRST, SPI_MODE0));
}
 
void loop() {
  Serial.println("looping");
   char c;
   digitalWrite(CS, LOW); // enable Slave Select
   byte c1 = 0b10110111;
   byte c2 = 0b01000000;
   SPI.transfer(c1);
   SPI.transfer(c2);
   digitalWrite(CS, HIGH); // disable Slave Select
   delay(200);
}
