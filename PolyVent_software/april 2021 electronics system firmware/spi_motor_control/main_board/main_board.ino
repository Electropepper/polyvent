#include "Arduino.h"
#include "SPI.h"
const int CS = 4;

//uncomment below to enable manual control
//byte * postitions = Serial_input_for_pos();

//uncomment below for constant values
byte conditionals;

uint16_t positions[2] = {5000, 2000};
uint16_t speeds[2] = {1000, 1000};
uint16_t accelerations[2] = {200, 200};

// send: int16 accel1, int16 accel2, int16 speed1, int16 speed2, int16 position_setpoint, int16 position_setpoint2, int16 conditions_to_ask_for_1_and_2
// bit 1: enable 1, bit 2: enable 2, bit 3: start_home, bit 4: is it homed?
uint16_t transmission[7] = {accelerations[0], accelerations[1], speeds[0], speeds[1], positions[0], positions[1], 0};
// bitwrite for conditions in transmission[6]

void setup()
{
  Serial.begin(115200);
  pinMode(CS, OUTPUT);
  digitalWrite(CS, HIGH);
  SPI.begin();
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));

  bitWrite(conditionals, 7, 1);
  bitWrite(conditionals, 6, 1);
  bitWrite(conditionals, 5, 1);
  bitWrite(conditionals, 4, 0);
  bitWrite(conditionals, 3, 1);
  bitWrite(conditionals, 2, 0);
}

void loop()
{

  // send data here
  digitalWrite(CS, LOW);
  
  for (int x = 0; x < 6; x++)
  {
    send_uint16_t(transmission[x]);
  }
  
  byte received_data = SPI.transfer(conditionals);
  
  digitalWrite(CS, HIGH);
  if(received_data != 7)
    Serial.println(received_data);
    
 // delay(20); //get rid of later
}

void send_uint16_t(uint16_t u)
{
  // Robert Reed's code
  SPI.transfer(*(((byte *)&u) + 0));
  byte b1 = *(((byte *)&u) + 0);

  SPI.transfer(*(((byte *)&u) + 1));
  byte b2 = *(((byte *)&u) + 1);
}
