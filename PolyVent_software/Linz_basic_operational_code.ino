/***************************************************************************
PolyVent Ventilator code
Written in Linz, october 2020.

humbly brought to you by 
     ************
     * Levi Türk*
     ************
     
credits to:

Jan Zünkler
Antal
Austin
Nathaniel

 ***************************************************************************/

#include <AccelStepper.h>
#include <ArduinoJson.h> 
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"
#include <math.h>

//Defining Sensor variables
#define BMP_SCK 18  //connects to SCK
#define BMP_MISO 19 //connects to SD0
#define BMP_MOSI 23 //connects to SD1
#define BMP_CS 26
#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BMP3XX bmp(BMP_CS, BMP_MOSI, BMP_MISO, BMP_SCK);

//Defining pins

#define stepPin 16
#define dirPin 17
#define stepPin2 26
#define dirPin2 25
#define motorInterfaceType 1
#define Relay1 (27)
#define Relay2 (14)//------ OK?????
#define Relay3 (15)
#define Relay4 (2)
#define Relay5 (21)
#define Relay6 (4)
#define limitswitch (22)  // FIX this one, might be taking the pin from a sensor CS
#define limitswitch2 (13)
#define enable_drivers_pin (5)

// Create a new instance of the AccelStepper class:
AccelStepper stepper1 = AccelStepper(motorInterfaceType, stepPin, dirPin);
AccelStepper stepper2 = AccelStepper(motorInterfaceType, stepPin2, dirPin2);
const int goal = 3000000;
int targetDistance = goal;
float pressure_amb = 985.00;
const int homeoffset = -300;
float Pressure_1;
const float bellows_dia = 11.2;
const float bellows_radius = bellows_dia/2;
const float bellows_area = PI*bellows_radius*bellows_radius;
const int steps_per_rotation = 1600;
const float distance_per_rotation = 0.8; //centimeters

void setup() {

  Serial.print("This function is running on core ");
  Serial.println(xPortGetCoreID());
  
  Serial.begin(115200);
  while (!Serial);                // sure OK???
  Serial.println("BMP388 test");  // sure OK???
  
  pinMode(Relay1, OUTPUT);  //bellow 1 out
  pinMode(Relay2, OUTPUT);  //bellow 1 in
  pinMode(Relay3, OUTPUT);  //bellow 2 out
  pinMode(Relay4, OUTPUT);  //bellow 2 in
  pinMode(Relay5, OUTPUT);  //patient out
  pinMode(Relay6, OUTPUT);  //air in
  Serial.println("Relays initialized.");

  Serial.println("Valves are all open");
  digitalWrite(Relay1, HIGH);
  digitalWrite(Relay2, HIGH);
  digitalWrite(Relay3, HIGH);
  digitalWrite(Relay4, HIGH);
  digitalWrite(Relay5, HIGH);
  digitalWrite(Relay6, HIGH);
 
  pinMode(limitswitch, INPUT);
  pinMode(limitswitch2, INPUT);

  Serial.println("Limit switches initialized.");
  delay(100);
  
  pinMode(enable_drivers_pin, OUTPUT);  //enables both drivers (EN+)
  digitalWrite(enable_drivers_pin, LOW);
  
  Serial.println("Stepper motor drivers enabled.");
  delay(100);

//////////////////Homing the first bellow//////////////////////////
///////////////////////////////////////////////////////////////////

  // Stepper motor stuff: set the maximum speed and acceleration:
  stepper1.setMaxSpeed(1000);
  stepper1.setAcceleration(1000);
  stepper1.moveTo(targetDistance);
  Serial.println("Stepper motor configured.");
  delay(500);
  Serial.println("");
  delay(500);

  Serial.println(digitalRead(limitswitch));
  //delay(1); //we needed this for it to work, but later we didn't? Superweird
  while (true){
    stepper1.run();
    //Serial.println(digitalRead(limitswitch)); //required for delay/denoising? Weird
    if(digitalRead(limitswitch) == LOW){
      Serial.println("Limit switch pressed, turning back");
      break;
    }
  }
    
  Serial.println("broken");
  stepper1.setCurrentPosition(0);
  stepper1.moveTo(homeoffset);
  
  while (stepper1.distanceToGo() < 0){
    stepper1.run();
    Serial.println(stepper1.distanceToGo());
  }

//////////////////Homing the second bellow/////////////////////////
///////////////////////////////////////////////////////////////////

  // Stepper motor stuff: set the maximum speed and acceleration:
  stepper2.setMaxSpeed(1000);
  stepper2.setAcceleration(1000);
  stepper2.moveTo(targetDistance);
  Serial.println("Stepper motor 2 configured.");
  delay(500);
  Serial.println("");
  Serial.println("Solenoid valve test starting...");
  delay(500);

  Serial.println(digitalRead(limitswitch2));
  //delay(1); //we needed this for it to work, but later we didn't? Superweird
  while (true){
    stepper2.run();
    //Serial.println(digitalRead(limitswitch)); //required for delay/denoising? Weird
    if(digitalRead(limitswitch2) == LOW){
      Serial.println("Limit switch pressed, turning back");
      break;
    }
  }
    
  Serial.println("broken");
  stepper2.setCurrentPosition(0);
  stepper2.moveTo(homeoffset);
  
  while (stepper2.distanceToGo() < 0){
    stepper2.run();
    Serial.println(stepper2.distanceToGo());
  }
}

void loop() {
  Serial.print("This function is running on core ");
  Serial.println(xPortGetCoreID());
  
  float volume_target = 400; //ml
  float respiratory_rate = 5; //per minute
  float inspiratory_fraction = 0.333;
  
  float distance_target = volume_target / bellows_area; //needs refinement for large displacements
  float rotation_target = distance_target / distance_per_rotation;
  int steps_target = rotation_target * steps_per_rotation;
  int acceleration = 30000;
  float total_cycle_time = 60 / respiratory_rate;
  float inspiratory_time = inspiratory_fraction*total_cycle_time;
  
  int steps_per_second = 1600; //testing; should be computed from above
  float D = inspiratory_time*inspiratory_time - 4*steps_target / acceleration;
  int required_speed = (acceleration/2) * (inspiratory_time - sqrt(D)); //not 100% sure if this is right

  int waiting_time = total_cycle_time - 2 * inspiratory_time;
  
//////////////// OPERATING BELLOW 1 ///////////////////
  
  Serial.println(inspiratory_time);
  Serial.println(acceleration);
  Serial.println(steps_target);
  Serial.println(required_speed);

  //Phase 1
  digitalWrite(Relay1, HIGH); //Bellow 1 out open
  stepper1.setMaxSpeed(required_speed);
  stepper1.setAcceleration(acceleration);
  stepper1.setCurrentPosition(0);  
  stepper1.moveTo(-steps_target); //down is negative
  while (stepper1.distanceToGo() < 0){
    stepper1.run();
  }


  //Phase 2
  digitalWrite(Relay1, LOW);  //bellow 1 out closed
  digitalWrite(Relay2, HIGH); //bellow 1 in open
  digitalWrite(Relay6, HIGH); //air in open
  digitalWrite(Relay5, HIGH); //patient exhale open
  stepper1.moveTo(0); //going back to homeoffset: filling the bellow with air
  while (stepper1.distanceToGo() > 0){
    stepper1.run();
    if(digitalRead(limitswitch) == LOW){
      Serial.println("Limit switch pressed, turning back");
      break;
    }
  }
  digitalWrite(Relay2, LOW); //bellow 1 in closed
  digitalWrite(Relay6, LOW); //air in closed
  digitalWrite(Relay5, LOW); //patient exhale closed
  delay(waiting_time * 1000);

//////////////// OPERATING BELLOW 2 ///////////////////

  //Phase 1
  digitalWrite(Relay3, HIGH); //Bellow 2 out open
  stepper2.setMaxSpeed(required_speed);
  stepper2.setAcceleration(acceleration);
  stepper2.setCurrentPosition(0);  
  stepper2.moveTo(-steps_target); //down is negative
  while (stepper2.distanceToGo() < 0){
    stepper2.run();
  }


  //Phase 2
  digitalWrite(Relay3, LOW);  //bellow 2 out closed
  digitalWrite(Relay4, HIGH); //bellow 2 in open
  digitalWrite(Relay6, HIGH); //air in open
  digitalWrite(Relay5, HIGH); //patient exhale open
  stepper2.moveTo(0); //going back to homeoffset: filling the bellow with air
  while (stepper2.distanceToGo() > 0){
    stepper2.run();
    
    if(digitalRead(limitswitch2) == LOW){
      Serial.println("Limit switch pressed, turning back");
      break;
    }
  }
  digitalWrite(Relay4, LOW); //bellow 2 in closed
  digitalWrite(Relay6, LOW); //air in closed
  digitalWrite(Relay5, LOW); //patient exhale closed
  delay(waiting_time * 1000);


}