#include <SPI.h>
#include <MultiStepper.h>
#include <AccelStepper.h>
#include <math.h>
AccelStepper mystepper(1, 8, 5); // 1 is the step dir driver mode, then is the step pin, then dir pin

#define DRIVE_MAGIC_NUMBER 137
#define PAUSE_MAGIC_NUMBER 44
char buff [50];
volatile byte indx;
volatile boolean process_string;
volatile boolean read_number;
volatile int bytes_of_number_read;
#define NUM_BYTES_IN_BUFFER 8

const float bellows_diameter_mm = 160;
const float bellows_radius = bellows_diameter_mm/2;
const float bellows_area_mmsq = PI*bellows_radius*bellows_radius;

const int steps_per_rotation = 48;//////1600 for actual pump module
const float distance_per_rotation_mm = 8; //millimeters

const float ml_per_rotation = (bellows_area_mmsq*distance_per_rotation_mm)/1000;
const float ml_per_step = ml_per_rotation/steps_per_rotation;

volatile byte bytes_in_number[NUM_BYTES_IN_BUFFER];

//////////////////polyvent code ///////////////////
void polyvent_constants(){
  
  /*
  float distance_target = volume_target / bellows_area; //needs refinement for large displacements
  float rotation_target = distance_target / distance_per_rotation;
  int steps_target = rotation_target * steps_per_rotation;
  float total_cycle_time = 60 / respiratory_rate;
  float inspiratory_time = inspiratory_fraction*total_cycle_time;
  */

  
    int acceleration = 30000;
  int steps_per_second = 1600; //testing; should be computed from above
//  float D = inspiratory_time*inspiratory_time - 4*steps_target / acceleration;
 // int required_speed = (acceleration/2) * (inspiratory_time - sqrt(D)); //not 100% sure if this is right
 // int waiting_time = total_cycle_time - 2 * inspiratory_time;
 //int  stepsPerml
}

void setup (void) {
   polyvent_constants();
   Serial.begin (115200);
Serial.print("ml per rotation: ");
Serial.println(ml_per_rotation);

Serial.print("ml per step: ");
Serial.println(ml_per_step);

Serial.print("bellow area: ");
Serial.println(bellows_area_mmsq);

Serial.print("distance per rotation: ");
Serial.println(distance_per_rotation_mm);

Serial.print("steps per rotation");
Serial.println(steps_per_rotation);

   mystepper.setAcceleration(3000);
   mystepper.setMaxSpeed(400);
  mystepper.setSpeed(200);
   pinMode(MISO, OUTPUT); // have to send on master in so it set as output
   SPCR |= _BV(SPE); // turn on SPI in slave mode
   indx = 0; // buffer empty
   process_string = false;
   SPI.attachInterrupt(); // turn on interrupt
   read_number = false;
}

ISR (SPI_STC_vect) // SPI interrupt routine 
{ 
   byte c = SPDR; // read byte from SPI Data Register
   if (!read_number) {
     if (indx < sizeof buff) {
        buff [indx++] = c; // save data in the next index in the array buff
        if (c == '\r')  { //check for the end of the word
          buff[indx] = '\0';
          process_string = true;
          read_number = true;
          bytes_of_number_read = 0;
        }
     }
   } else { // we are either processing a string, or a number.
      if (bytes_of_number_read < NUM_BYTES_IN_BUFFER) {
        bytes_in_number[bytes_of_number_read] = c;
        bytes_of_number_read++;
      }
      if (bytes_of_number_read >= NUM_BYTES_IN_BUFFER) {
        read_number = false;
      }
   }
}
uint16_t readUint16_t(int loc_in_buffer, byte *buff) {
  int n = loc_in_buffer;
  uint16_t v;
  *(((byte *) &v)) = buff[n];
  *(((byte *) &v)+1) = buff[n+1];
  return v; 
}

void loop (void) {
  
   if (process_string) {
      process_string = false; //reset the process
 //     Serial.println (buff); //print the array on serial monitor
      indx= 0; //reset button to zero
   }
   if (bytes_of_number_read >= NUM_BYTES_IN_BUFFER) {
      uint16_t drive = readUint16_t(0,bytes_in_number); 
      switch (drive) {
      case DRIVE_MAGIC_NUMBER: {
        uint16_t flow_mlpm = readUint16_t(2,bytes_in_number);
        uint16_t at_pressure_cmH2O_tenths = readUint16_t(4,bytes_in_number);
        uint16_t time_ms = readUint16_t(6,bytes_in_number);
        polyvent_drive(flow_mlpm, at_pressure_cmH2O_tenths, time_ms); 
      }
        break;
      case PAUSE_MAGIC_NUMBER: {
        uint16_t pause_ms = readUint16_t(2,bytes_in_number);
        polyvent_pause_and_prep(pause_ms);
      }
        break;
      default:
        Serial.print("Bad Magic Number: ");
        Serial.println(drive);
        break;
      }  
      bytes_of_number_read = 0; 
  }  
}

long position = 0;
// Antal, you drivers go here....you can probably ignore the pressure,
// unless you want to throw a warning if it is to high.
int polyvent_drive(uint16_t flow_mlpm, uint16_t at_pressure_cmH2O_tenths, uint16_t time_ms) {
  
    Serial.print("drive :");  
    Serial.println("flow, pressure, time_ms");
    Serial.print(" ");
    Serial.print(flow_mlpm);
    Serial.print("ml per min ");
    Serial.print(at_pressure_cmH2O_tenths/10.0);
    Serial.print("cm H2O ");
    Serial.print(time_ms);
    Serial.println("ms ");
     
    float ml_per_s = flow_mlpm/60.0;
    float step_per_s = ml_per_s/ml_per_step;
    Serial.print("steps per second ");
    Serial.println(step_per_s );
    long start = millis();

    mystepper.setSpeed(step_per_s);
    float steps_to_move = (step_per_s/1000)*time_ms;
    
       Serial.print("steps to move  ");
     Serial.println(steps_to_move );
     
    mystepper.move((long)steps_to_move);
    while(millis()< (start + time_ms)){
    mystepper.run();
    }
   



}
// This is probably a means for you to retract the bellows...
int polyvent_pause_and_prep(uint16_t time_ms) {

    Serial.print("pause : ");  
    Serial.print(time_ms);
    Serial.println("ms ");  
    position = 0;
   // mystepper.moveTo(position);
    mystepper.runToNewPosition(position);

    
}
