// runs on the polyvent valves board
//#include "Arduino.h"
#include "SPI.h"

byte received_signal [2];
volatile byte indx;
volatile boolean process;

//pin mapping, in order, fist controls solenoid zero, second controls solenoid 1, etc
const int permanent_pin_mapping [10] = {2, 3, 4, 5, 6, 7, 8, 9, 14, 15};

void setup (void) {
   Serial.begin(9600);

   pinMode(MISO, OUTPUT); // have to send on master in so it set as output

   SPCR |= _BV(SPE); // turn on SPI in slave mode
   SPI.attachInterrupt(); // turn on interrupt

   for(int x = 0; x < sizeof permanent_pin_mapping; x++){
      pinMode(permanent_pin_mapping[x], OUTPUT);
      digitalWrite(permanent_pin_mapping[x], LOW);
   }
   indx = 0; // received_signaler empty
   process = false;
}

ISR (SPI_STC_vect) // SPI interrupt routine 
{ 
   byte c = SPDR; // read byte from SPI Data Register
   if (indx < sizeof received_signal) {
      received_signal [indx++] = c; // save data in the next index in the array received_signal
      if (c == '\n') // end of transmission
        process = true;
   }
}


void loop (void) {
 // Serial.println("looping");
   if (process) {
      process = false; //reset the process
      indx= 0; //reset button to zero
   }
 bool values[10];
   byte first_byte_decomposable = received_signal[0];
   byte second_byte_decomposable = received_signal[1];
   for(int x = 0; x<8; x++){
     values[x] = bitRead(first_byte_decomposable, 7-x);
   }
   for(int x = 0; x<2; x++){
      values[x + 8] = bitRead(second_byte_decomposable, 7-x);
   }
//   for(int x = 0; x<10; x++){
//      Serial.println(values[x]);
//   }
   
   for(int valve_num = 0; valve_num < 10; valve_num++){
      digitalWrite(permanent_pin_mapping[valve_num], values[valve_num]);
      //Serial.println(values[valve_num]);
   }
}
