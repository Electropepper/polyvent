#include <SPI.h>

#define DRIVE_MAGIC_NUMBER 137
#define PAUSE_MAGIC_NUMBER 44

// This is a rough approach to a ventilation mode..
// VentOS will eventually replace this...
const int bpm = 10;
const int inspiration_part = 1;
const int expiration_part = 2;
const int target_pressure = 200; // tenths of cm H2O
const int total_breath_ms = 60000 / bpm;
const int expiration_ms = (total_breath_ms * expiration_part) / (inspiration_part + expiration_part);
const int inspiration_ms = (total_breath_ms * inspiration_part) / (inspiration_part + expiration_part);
const int preferred_drive_chunk_ms = 200;

void setup (void) {
   Serial.begin(115200); //set baud rate to 115200 for usart
   digitalWrite(SS, HIGH); // disable Slave Select
   SPI.begin ();
   SPI.setClockDivider(SPI_CLOCK_DIV8);//divide the clock by 8
}
void send_one_byte(byte b) {
    digitalWrite(SS, LOW); // enable Slave Select
    SPI.transfer(b); 
 //   Serial.println(b);
    digitalWrite(SS, HIGH); // disable Slave Select
}

void send_two_bytes(byte b1, byte b2) {
    digitalWrite(SS, LOW); // enable Slave Select
    SPI.transfer(b1); 
 //   Serial.println(b1);
    SPI.transfer(b2); 
 //   Serial.println(b2);
    digitalWrite(SS, HIGH); // disable Slave Select
}
void send_uint16_t(uint16_t u) {
    digitalWrite(SS, LOW); // enable Slave Select
    SPI.transfer(*(((byte *) &u)+0)); 
    byte b1 = *(((byte *) &u)+0);
    Serial.println(b1);
    SPI.transfer(*(((byte *) &u)+1)); 
    byte b2 = *(((byte *) &u)+1);
    Serial.println(b2);
    digitalWrite(SS, HIGH); // disable Slave Select
}

// Rob begins coding here....
// Here we implement a "PolyVent driver implementation"
// for our standard driver...this will make the SPI calls
// to be received by the server/slave. 

// The basic implementation is to pack these into 8-byte buffers
// consisting of 4 16 bit unsignd integers.
// The first 2 bytes specify which function is called.
// The next 3 2-byte values represent the parameters.
int drive(uint16_t flow_mlpm, uint16_t at_pressure_cmH2O_tenths, uint16_t time_ms);
int pause_and_prep(uint16_t timer_ms);

void sendStartSignal() {
  char c;
  digitalWrite(SS, LOW); // enable Slave Select
  // send test string
  for (const char * p = "rx!\r" ; c = *p; p++) 
  {
      SPI.transfer (c);
      Serial.print(c);
  }
  Serial.println();  
  digitalWrite(SS, HIGH); // disable Slave Select
}

void loop (void) {
  uint16_t pressure = 200; // (20 cm H2O);
  uint16_t flow = 20000; // This is ml/min; we might have to adjust our scale
  uint16_t time_ms = 1000; // we'll drive 3 times and then rest...
  for(int i = 0; i < (inspiration_ms / preferred_drive_chunk_ms); i++) {
    sendStartSignal();
    drive(flow, pressure, preferred_drive_chunk_ms);
    delay(200);    
  }
  Serial.println("exited loop");
  sendStartSignal();
  pause_and_prep(expiration_ms);
  delay(expiration_ms); 
}

// suggested implementation
int drive(uint16_t flow_mlpm, uint16_t  at_pressure_cmH2O_tenths, uint16_t  time_ms) {
  uint16_t v = DRIVE_MAGIC_NUMBER; // magic number for "drive"
  send_uint16_t(v);
  send_uint16_t(flow_mlpm);
  send_uint16_t(at_pressure_cmH2O_tenths);
  send_uint16_t(time_ms);
}
 
int pause_and_prep(uint16_t time_ms) {
  uint16_t v = PAUSE_MAGIC_NUMBER; // magic number for "drive"
  send_uint16_t(v);
  send_uint16_t(time_ms);
  // Sending zeroes prevents the dange of an aciddental collison with a meaningful value.
  send_uint16_t(0);
  send_uint16_t(0);
}
